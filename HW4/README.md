# CS 5343: Assignment 4 #

The objective of this assignment is to create a graph representation (in my case as an adjacency matrix) and to incorporate DFS and BFS traversals on that representation.

### Compilation Instructions ###

1) ssh into username@cslinux1.utdallas.edu

2) Make sure that you have the g++ compiler. If not, install it.

3) Compile program file by typing 'g++ A4.cpp' into Linux environment.

4) An 'a.out' file will automatically be created.

5) Type './a.out' underneath the 'g++ A4.cpp' statement.
