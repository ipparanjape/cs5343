# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <algorithm>
# include <queue>

using namespace std;

void DFS(int A[][10], bool B[10], int v_start)
{
  cout << v_start << " ";
  B[v_start] = true;
  for (int i = 0; i < 10; i++)
  {
     if (A[v_start][i] == 1 && B[i] == false)
     {
        DFS(A, B, i);
     }
  }
}  

void BFS(int A[][10], bool B[10], queue<int> f	, int v_start)
{
   f.push(v_start);
   B[v_start] = true;
   while (!f.empty())
   {
      cout << f.front() << " ";
      int key = f.front();
      f.pop();
      for (int i = 0; i < 10; i++)
      {
         if (A[key][i] == 1 and B[i] == false)
	 {
	    f.push(i);
	    B[i] = true;
	 }
      }
   }
}   

int main()
{
   int adj_mat[10][10] = {{0, 1, 1, 0, 0, 0, 0, 0, 0, 0}, {1, 0, 0, 0, 0, 1, 1, 0, 0, 0}, {1, 0, 0, 1, 1, 0, 0, 0, 0, 0},{0, 0, 1, 0, 1, 0, 0, 0, 0, 0}, {0, 0, 1, 1, 0, 1, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 0, 1, 0, 0, 0}, {0, 1, 0, 0, 0, 1, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 0, 0}}; 
   int n = sizeof(adj_mat)/sizeof(adj_mat[0]);
   cout << "Names of vertices: " << endl;
   
   // Loop for printing names of vertices
   for (int i = 0; i < n; i++)
   {
       cout << i << " ";
   }
   cout << endl;

   // Loop for printing edges
   int e = 0;
   cout << "Names of edges: " << endl;
   for (int i = 0; i < n; i++)
   {
      for (int j = 0; j < n; j++)
      {
         if(adj_mat[i][j] == 1)
	 {
	    cout << "(" << i << "," << j << ") ";
	    e++;
	 }
      }
   }
   cout << endl;
   cout << "DFS Traversal of Graph: "; 
   bool track[n];
   DFS(adj_mat, track, 0); // Start from vertex 0
   cout << endl;
   
   // Begin BFS Traversal setup
   cout << "BFS Traversal of Graph: ";
   queue <int> q;
   
   bool track2[n];
   BFS(adj_mat, track2, q, 0); // Start from vertex 0
   cout << endl;
   return 0;
}


