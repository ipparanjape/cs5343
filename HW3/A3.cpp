# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <algorithm>
using namespace std;

void heapify15(int A[], int size)
{
   int maxParentIndex;
   if (size % 2 == 0) {maxParentIndex = (size - 3)/2;}
   else {maxParentIndex = (size - 2)/2;}
   int n = maxParentIndex;
   while (maxParentIndex >= 0)
   {
      if (A[maxParentIndex] > A[2*maxParentIndex+1] || A[maxParentIndex] > A[2*maxParentIndex+2]) // if parent is greater than any of its children
      {
         if(A[2*maxParentIndex+1] > A[2*maxParentIndex+2]) // if right child is smaller
	 {
	    int tmp = A[maxParentIndex];
	    A[maxParentIndex] = A[2*maxParentIndex+2];
	    A[2*maxParentIndex+2] = tmp; // swap parent with its right child
	 }
         if(A[2*maxParentIndex+1] < A[2*maxParentIndex+2]) // if left child is smaller
	 {
	    int tmp = A[maxParentIndex];
	    A[maxParentIndex] = A[2*maxParentIndex+1];
            A[2*maxParentIndex+1] = tmp; // swap parent with its left child
	 } 
      }	 
      maxParentIndex -= 1;
   }
   maxParentIndex = 0;
   while (maxParentIndex <= n)
   {
      if (A[maxParentIndex] > A[2*maxParentIndex+1] || A[maxParentIndex] > A[2*maxParentIndex+2])
      {
         if(A[2*maxParentIndex+1] > A[2*maxParentIndex+2]) // if right child is smaller
         {
	    int tmp = A[maxParentIndex];
	    A[maxParentIndex] = A[2*maxParentIndex+2];
	    A[2*maxParentIndex+2] = tmp; // swap parent with its right child
	 }

         if(A[2*maxParentIndex+1] < A[2*maxParentIndex+2]) // if left child is smaller
	 {
	    int tmp = A[maxParentIndex];
	    A[maxParentIndex] = A[2*maxParentIndex+1];
	    A[2*maxParentIndex+1] = tmp; // swap parent with its left child
	 }
      }
      maxParentIndex += 1;
   }
   maxParentIndex -= 1;
   while (maxParentIndex >= 0)
   {
      if (A[maxParentIndex] > A[2*maxParentIndex+1] || A[maxParentIndex] > A[2*maxParentIndex+2]) // if parent is greater than any of its children
      {
         if(A[2*maxParentIndex+1] > A[2*maxParentIndex+2]) // if right child is smaller
	 {
	    int tmp = A[maxParentIndex];
	    A[maxParentIndex] = A[2*maxParentIndex+2];
	    A[2*maxParentIndex+2] = tmp; // swap parent with its right child
	 }
         if(A[2*maxParentIndex+1] < A[2*maxParentIndex+2]) // if left child is smaller
	 {
	    int tmp = A[maxParentIndex];
	    A[maxParentIndex] = A[2*maxParentIndex+1];
            A[2*maxParentIndex+1] = tmp; // swap parent with its left child
	 } 
      }	 
      maxParentIndex -= 1;
   }
}

void heapifySubtree(int A[], int n, int p)
{
   int parent = p; // Parent index
   int lchild = 2*p + 1; // Left child index
   int rchild = 2*p + 2; // Right child index
   if (lchild < n and A[lchild] > A[parent]) {parent = lchild;} // Replace parent with left child
   if (rchild < n and A[rchild] > A[parent]) {parent = rchild;}  // Replace parent with right child
   if (parent != p) // if switching occurs in one of the above if statements...
   {
      int tmp = A[p];
      A[p] = A[parent];
      A[parent] = tmp;
      heapifySubtree(A, n, parent);
   }
}

void heapsort(int A[], int size)
{
   int maxParentIndex;
   if (size % 2 == 0) {maxParentIndex = (size - 3)/2;}
   else {maxParentIndex = (size - 2)/2;}
   
   while (maxParentIndex >= 0)
   {
      heapifySubtree(A, size, maxParentIndex);
      maxParentIndex -= 1;
   }

   /* 
   cout << "Heapified Array: ";
   for (int i = 0; i < size; i++)
   {
       cout << A[i] << " ";
   }
   cout << endl;
   */

   int counter = size - 1;
   while (counter >= 0)
   {
      int tmp = A[0];
      A[0] = A[counter];
      A[counter] = tmp;
      heapifySubtree(A, counter, 0);
      counter -= 1;
   }
   reverse (A, A + size);
}   

int main()
{
   int heapArray[15];
   srand (time(NULL)); // Randomizing integers
   for (int i = 0; i < 15; i++)
    {
      int tmp;
      tmp = rand() % 100 + 1; // Generates random integer between 1 and 100
      heapArray[i] = tmp;  // Creates a new node in a linked list
    }
   // int heapArray[15] = {8, 4, 7, 1, 3, 2, 5 , 15, 14, 10, 11, 6, 13, 9, 12}; // If you want a random array, please comment this out.
   int arrayLength = sizeof(heapArray)/sizeof(heapArray[0]);
   cout << "Array length: " << arrayLength << endl;

   cout << "Original Array: ";
   for (int i = 0; i < arrayLength; i++)
   {
      cout << heapArray[i] << " ";
   }
   cout << endl;
   
   heapify15(heapArray, arrayLength);

   cout << "Min Heap Array: ";
   for (int i = 0; i < arrayLength; i++)
   {
      cout << heapArray[i] << " ";
   }
   cout << endl;

   heapsort(heapArray, arrayLength);

   cout << "Heap Sorted Array: ";
   for (int i = 0; i < arrayLength; i++)
   {
      cout << heapArray[i] << " ";
   }
   cout << endl;


   return 0;
}
