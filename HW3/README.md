# CS 5343: Assignment 1 #

The objective of this assignment is to traverse a linked list of size 15 and sort the linked list by node values. 

### Compilation Instructions ###

1) ssh into username@cslinux1.utdallas.edu

2) Make sure that you have the g++ compiler. If not, install it.

3) Compile program file by typing 'g++ A1.cpp' into Linux environment.

4) An 'a.out' file will automatically be created.

5) Type './a.out' underneath the 'g++ A1.cpp' statement.
