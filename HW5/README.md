# CS 5343: Assignment 5 #

The objective of this assignment is to find the DFS and BFS topological order of two graphs, one of which has a cycle. Cycle 1 (represented by the adjacency matrix g1) has a cycle whereas Cycle 2 (represented by the adjacency matrix g2) does not. Note that the program does appear a little glitchy because it tends to run properly usually after you open the CPP file using "vi" and then compile as follows.

If it doesn't work the first time. Simply type "./a.out" into the terminal again.

### Compilation Instructions ###

1) ssh into username@cslinux1.utdallas.edu

2) Make sure that you have the g++ compiler. If not, install it.

3) Compile program file by typing 'g++ A5.cpp' into Linux environment.

4) An 'a.out' file will automatically be created.

5) Type './a.out' underneath the 'g++ A5.cpp' statement.
