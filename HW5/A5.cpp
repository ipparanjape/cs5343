# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <algorithm>
# include <queue>
# include <stack>
# include <vector>

using namespace std;

void topoDFS(int *g, bool visited[], bool done[], /* int backOrdering[], */ stack<int> &stk, int i, int n /* , int count */)
{
   visited[i] = true;
   for (int j = 0; j < n; j++)
   {
      /*
            // cout << *((g+i*n+j)) << " "; // Test - prints contents of the adjacency matrix
      */
      if (*((g+i*n)+j) == 1 and !visited[j])
      {
         topoDFS((int *)g, visited, done, /* backOrdering, */ stk, j, n /* , count */);
	 // done[i] = true;
      }
      if ((*((g+i*n)+j) == 1 and visited[j]) and !done[j])
         {
            cout << endl;
	    cout << "There is a cycle!" << endl;
	    // if (n == 8) {int k = i + 1; int l = j + 1; cout << "There is a cycle from " << k << " to " << l << ".\n";}
	    // if (n == 14) {char c = 'm' + i; char d = 'm' + j; cout << "There is a cycle from " << c << " to " << d << ".\n";}
            // cout << "There is a cycle from vertex " << i << "to  << endl;
	    break;
         }
  }
   done[i] = true;
   stk.push(i);
   // backOrdering[count] = stk.top();
   // count += 1;
}

int countPreds(int* g, int i, int n)
{
   int countNegs = 0;
   for (int j = 0; j < n; j++)
   {
      if(*((g+i*n) + j) == -1) {countNegs += 1;}
   }
   return countNegs;
}   

void topoBFS(int* g, int predCount[], bool visited[], queue<int> &f,  int i, int n)
{
   for (int j = 0; j < n; j++)
   {
      if (predCount[j] == 0 & !visited[j]) 
      {
         f.push(j);
      }
   }   
      
   while (!f.empty())
   {
      if (n == 8) {cout << f.front() + 1 << " ";}
      if (n == 14)
      {
        char c = 'm' + f.front();
	cout << c << " ";
      }	
      int key = f.front();
      visited[key] = true;
      f.pop();
      for (int j = 0; j < n; j++)
      {
         if (*((g+key*n) + j) == 1 and visited[j] == false)
	 {
	    predCount[j] -= 1;
            if (predCount[j] == 0){f.push(j);}
	    visited[j] == true;
	 }
      }
   }
   for (int j = 0; j < n; j++)
   {
      if(predCount[j] != 0 and !visited[j])
      {
         cout << endl;
	 cout << "There is a cycle!";
	 break;
      }
   }   
}

int main()
{
   // int g0[6][6] = {{0, 0, 0, 0, -1, -1}, {0, 0, 0, -1, -1, 0}, {0, 0, 0, 1, 0, -1}, {0, 1, -1, 0, 0, 0}, {1, 1, 0, 0, 0, 0}, {1, 0, 1, 0, 0, 0}};
   // int n = sizeof(g0)/sizeof(g0[0]);
   // cout << "Example Graph DFS Topological Order: " << endl;
   // bool visited0[n];
   // stack<int> stack0;
   stack<int> stack1;
   stack<int> stack2;
   // int backOrdering0[n];
   // int backOrdering1[n];
   // int backOrdering2[n];

   // Implement topological DFS row by row
   // int ct = 0;
   /*
   for (int i = 0; i < n; i++)
   {
      if (!visited0[i])
      {
         topoDFS((int *)g0, visited0, stack0, i, n);
      }
   }
   
   while (!stack0.empty())
   {
      cout << stack0.top() << " ";
      stack0.pop();
   }   
   cout << endl;
   */

   int g1[8][8] = {{0, 1, 0, 0, 1, 1, 0, 0}, {-1, 0, 1, 0, 1, 0, 1, 0}, {0, -1, 0, 1, 0, 0, 0, 0}, {0, 0, -1, 0, 1, 0, -1, 0}, {-1, -1, 0, -1, 0, -1, 1, 1}, {-1, 0, 0, 0, 1, 0, 0, 1}, {0, -1, 0, 1, -1, 0, 0, 1}, {0, 0, 0, 0, -1, -1, -1, 0}};
   int g2[14][14] = {{0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0}, {0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0}, {0, -1, 0, -1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0}, {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1}, {-1, -1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, {-1, 0, -1, 0, 0, 0, -1, 0, 1, 0, 0, 0, 1, 0}, {0, 0, -1, -1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, 0, 0}, {0, -1, 0, 0, 0, -1, 0, 1, 0, 0, 0, 0, 0, 0}, {0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 1, 1, -1, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 1}, {-1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0}, {0, 0, 0, 0, 0, -1, 0, 0, 0, 1, 0, 0, 0, 0}, {0, 0, 0, -1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0}};
   
   int g3[14][14];
   int n1 = sizeof(g1)/sizeof(g1[0]);
   int n2 = sizeof(g2)/sizeof(g2[0]);
   for (int i = 0; i < n2; i++)
   {
      for (int j = 0; j < n2; j++)
      {
         if(g2[i][j] + g2[j][i] != 0)
         {
            char c1 = 'm' + i;
	    char c2 = 'm' + j;
            cout << "Mismatch at " << c1 << " and " << endl;
         }
      }
   }   

   bool visited1[n1];
   bool visited2[n2];
   
   bool done1[n1];
   bool done2[n2];
   cout << "DFS Topological Ordering for Graph 1: " << endl;
   for (int i = 0; i < n1; i++)
   {
      if (!visited1[i])
      {
         topoDFS((int *)g1, visited1, done1, stack1, i, n1);
      }
   }

   while (!stack1.empty())
   {
     cout << stack1.top() + 1 << " ";
     stack1.pop();
   }
   cout << endl;
   
   cout << "BFS Topological Ordering for Graph 1: " << endl;
   int predCount[n1];
   for (int i = 0; i < n1; i++)
   {
      predCount[i] = countPreds((int *)g1, i, n1);
   } 
   bool v[n1];
   bool v2[n2];
   queue<int> q;
   queue<int> q2;
   for (int i = 0; i < n1; i++)
   {
     if (!v[i])
     {
        topoBFS((int *) g1, predCount, v, q,  i, n1);
     }
   }  
   cout << endl;
   
   cout << "BFS Topological Ordering for Graph 2: " << endl;
   int predCount2[n2];
   for (int i = 0; i < n2; i++)
   {
      predCount[i] = countPreds((int *)g2, i, n2);
   }

   int i = 2;
   if(!v2[i])
   {
      topoBFS((int *)g2, predCount, v2, q2, i, n2);
   }
   cout << endl;

   cout << "DFS Topological Ordering for Graph 2: " << endl;
   for (int i = 0; i < n2; i++)
   {
      if (!visited2[i])
      {
         topoDFS((int *)g2, visited2, done2, stack2, i, n2);
      }
   }

   while (!stack2.empty())
   {
     char c = stack2.top() + 'm';
     cout << c << " ";
     stack2.pop();
   }
   cout << endl;

   
   /*
   // Check for cycle
   // If the adjacency matrix is multiplied by itself and one of the diagonal
   // elements is not equal to zero, then there is a cycle, because it implies 
   // a loop to itself. 

   for (int i = 0; i < n2; i++)
   {
      for (int j = 0; j < n2; j++)
      {
         int mat_sum = 0;
	 for (int k = 0; k < n2; k++)
	 {
	   mat_sum += g2[i][k]*g2[k][j];
	 }
	 g3[i][j] = mat_sum;
      }
   }

   for (int i = 0; i < n2; i++)
   {
      if (g3[i][i] != 0)
      {
         cout << "There is a cycle." << endl;
	 return 0;
      }
   }   
   
   for (int i = 3; i < n2; i++)
   {
      if (!visited2[i])
      {
         topoDFS((int *)g2, visited2, stack2, i, n2);
      }
   }

   while (!stack2.empty())
   {
     char c = 'm' + stack2.top();
     cout << c << " ";
     stack2.pop();
   }
   cout << endl;
   */
   return 0;
}   
