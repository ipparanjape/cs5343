# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
using namespace std;

struct NODE
{  
   int val;
   NODE* lchild;
   NODE* rchild;
   NODE* parent;
   NODE(int value)
   {
     val = value;
     lchild = parent = rchild = NULL;
   }
};

NODE* makeNode(int value)
{
  NODE* tmp = new NODE(value);
  return tmp;
}

NODE* insert_node(NODE* tmp, int a)
{
  if(tmp == NULL) {return makeNode(a);}
  if(tmp->val < a)
  {     
     tmp->rchild = insert_node(tmp->rchild,a);
     tmp->rchild->parent = tmp;
  }
  if(tmp->val > a)
  {
     tmp->lchild = insert_node(tmp->lchild,a);
     tmp->lchild->parent = tmp;
  }
  return tmp;
}  

NODE* minNode(NODE* tmp)
{
  NODE* now = tmp;
  while (now != NULL && now->lchild != NULL)
  {
     now = now->lchild;
  }
  return now;
}

NODE* delete_Node(NODE* tmp, int a)
{
  if(tmp == NULL) {return tmp;}
  else if(tmp->val > a)
  {
     tmp->lchild = delete_Node(tmp->lchild, a);
  }
  else if(tmp->val < a)
  {
     tmp->rchild = delete_Node(tmp->rchild, a);
  }
  else
  {
     if(tmp->lchild == NULL)
     {
        NODE* n = tmp->rchild;
	tmp->val = 0;
	tmp->rchild->parent = NULL;
	tmp->rchild = NULL;
	return n;
     }

     else if (tmp->rchild == NULL)
     {
         NODE* n = tmp->lchild;
	 tmp->val = 0;
	 tmp->lchild->parent = NULL;
	 tmp->rchild = NULL;
	 return n;
     }

     NODE* n = minNode(tmp->rchild);
     tmp->val = n->val;
     tmp->rchild = delete_Node(tmp->rchild, tmp->val);
  }
  return tmp;
}  


void inOrderDisplay(NODE* tmp)
{
  if(tmp == NULL){return;}
  else
  {
     inOrderDisplay(tmp->lchild);
     cout << tmp->val << " ";
     inOrderDisplay(tmp->rchild);
  }
}				       	

int main()
{
  int tArray[15] = {50, 40, 80, 20, 45, 60, 100, 70, 65, 42, 44, 30, 25, 35, 33};
  NODE* root = NULL;
  for(int i = 0; i < 15; i++)
  {
    root = insert_node(root, tArray[i]);
  }
  inOrderDisplay(root);
  cout << endl;
  root = delete_Node(root, 50);
  inOrderDisplay(root);
  cout << endl;
  root = delete_Node(root, 40);
  inOrderDisplay(root);
  cout << endl;
  root = delete_Node(root, 35);
  inOrderDisplay(root);
  cout << endl;
  root = delete_Node(root, 65);
  inOrderDisplay(root);
  cout << endl;
  return 0;
}
