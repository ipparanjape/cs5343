# CS 5343: Assignment 6 #

The goal of this assignment is to implement a hash table that maps a key to a word taken from an array of 20 words. If the load factor exceeds 0.5, the table is rehashed as a modified hash function is used. Below are the has functions used before and after rehashing:

Before: key = (a1 + a2 + a3) mod tableSize
After: key = (a1 + a2 + a3) mod newTableSize

In these expressions, a1, a2, and a3 are the alphabet ordering value of the first three letters of each word in the array. For example, the letter 'a' has a value of 1, 'b' as a value of 2, 'c' has a value of 3, etc. The original value for tableSize is 31. After rehashing, newTableSize becomes 61, since that is the closest prime number to two times the original table size.

### Compilation Instructions ###

1) ssh into username@cslinux1.utdallas.edu

2) Make sure that you have the g++ compiler. If not, install it.

3) Compile program file by typing 'g++ -std=c++11 A6.cpp' into Linux environment.

4) An 'a.out' file will automatically be created.

5) Type './a.out' underneath the 'g++ -std=c++11 A6.cpp' statement.
