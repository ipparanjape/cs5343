# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <math.h>
// # include <algorithm>
// # include <queue>
// # include <stack>
# include <vector>
# include <unordered_map>

using namespace std;

bool keyExists(int key, unordered_map<int, string> m)
{
   unordered_map<int, string>::iterator it = m.find(key);
   if (m.find(key) == m.end()) {return false;}
   else {return true;}
}   

int main()
{
   unordered_map<int, string> hTable;
   string pledge[20]  = {"pledge", "uphold", "objects", "circle", "foster", "and", "goodwill", "towards", "others", "through", "service", "develop", "all", "people", "dedicate", "myself", "humanity", "live", "love", "serve"};
   int n = sizeof(pledge)/sizeof(pledge[0]);
   int table_size = 31;
   const double LOAD_LIMIT = 0.5;
   int collisions1, collisions2;

   // 1st Hashing Scheme: Sum of letter order values of first and 
   // second letters of the word mod 31 (i.e. 'a' = 1, 'b' = 2, ..., 'z' = 26)
   // 2nd Hashing Scheme: Sum of letter order values of first three
   // letters of the word mod 62 (i.e. 'a' = 1, 'b' = 2, ..., 'z' = 26)
   
   for (int i = 0; i < n; i++)
   {
      double load = (double)(i+1)/(double)table_size;
      if (load > LOAD_LIMIT) {break;}
      int quad_sum = 0;
      int quad_marker = 0;

      char c1 = pledge[i].at(0);
      char c2 = pledge[i].at(1);
      char c3 = pledge[i].at(2);
      int a1 = (int)c1 - 96; int a2 = (int)c2 - 96; int a3 = (int)c3 - 96;
      int key = (a1 + a2) % table_size;
    
      if (!keyExists(key, hTable))
      {
         hTable.insert(make_pair(key, pledge[i]));
      }
      else
      {
         int orig_key = key;
         while (keyExists(key, hTable))
	 {
	    quad_marker += 1;
	    collisions1 += 1;
	    quad_sum = pow(quad_marker, 2);
	    key = (orig_key + quad_sum) % 31;
	 }
	 hTable.insert(make_pair(key, pledge[i]));
	 quad_marker = 0;
	 quad_sum = 0;
      } 
   }   
   
   cout << "Original Hashing Scheme: " << endl;
   for (auto x: hTable)
   {
      cout << x.first << " " << x.second << endl;
   }
   cout << "Total number of collisions: " << collisions1 << endl;

   unordered_map<int, string> hTable2;
   table_size = 61; // Closest prime number to 2*table_size

   for (int i = 0; i < n; i++)
   {
      int quad_marker = 0;
      int quad_sum = 0;
      char c1 = pledge[i].at(0);
      char c2 = pledge[i].at(1);
      char c3 = pledge[i].at(2);
      int a1 = (int)c1 - 96; int a2 = (int)c2 - 96; int a3 = (int)c3 - 96;
      int key = (a1 + a2 + a3) % table_size;
 
      if (!keyExists(key, hTable2))
      {
         hTable2.insert(make_pair(key, pledge[i]));
      }
      else
      {
         int orig_key = key;
         while (keyExists(key, hTable2))
	 {
	    quad_marker += 1;
	    collisions2 += 1;
	    quad_sum = pow(quad_marker, 2);
	    key = (orig_key + quad_sum) % table_size;
	 }
	 hTable2.insert(make_pair(key, pledge[i]));
	 quad_marker = 0;
	 quad_sum = 0;
      } 
   }   

   cout << endl << "Rehashed Scheme: " << endl;
   for (auto x: hTable2)
   {
      cout << x.first << " " << x.second << endl;
   }
   cout << "Total number of collisions: " << collisions2 << endl;

   return 0;
}   
