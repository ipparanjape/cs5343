# include <iostream>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
using namespace std;

struct NODE
{
   int val;
   NODE* next;
};

class linkList
{
   NODE* head;
   NODE* tail;

   public:
   linkList()
   {
      head = NULL;
      tail = NULL;
   }
   void makeNode(int value)
   {
      NODE *tmp = new NODE;
      tmp->val = value;
      tmp->next = NULL;
      if (head == NULL)
      {
        head = tmp;
	tail = tmp;
	tmp = NULL;
      }
      else
      {
        tail->next = tmp;
        tail = tmp;
      }
   }
  
  void showList()
  {
     NODE *tmp;
     tmp = head;
     while (tmp != NULL)
     {
        cout << tmp->val << " ";
	tmp = tmp->next;
     }
  }

  void insertBefore(NODE* n, NODE* ref, NODE* tmp) // tmp is NODE 3 (previous node), n is NODE 4 (node to be inserted), ref is NODE 5 (reference node)
  {
    if(tmp->next == ref)
    {
      n->next = tmp->next;
      tmp->next = n;
    }
    else
    {
      insertBefore(n,ref,tmp->next);
    }
  }

  void deleteNode(NODE *tmp, NODE *n)
  {
    if(tmp->next == n)
    {
      tmp->next = n->next;
    }
    else
    {
      deleteNode(tmp->next, n);
    }
  }

  void selectionSort()
  {
    NODE* min;
    NODE* current;
    NODE* after;
    int temp;

    current = head;
    while (current != NULL)
    {
      min = current;
      after = current->next;
      while (after != NULL)
      {
        if(after->val < min->val)
	{ 
	  min = after;
	}
	after = after->next;
      }
      temp = current->val;
      current->val = min->val;
      min->val = temp;
      current = current->next;
   }
  }
	
};

int main()
{
   linkList a; // Declaring a new linked list object
   srand (time(NULL)); // Randomizing integers
   for (int i = 0; i < 15; i++)
   {
      int tmp;
      tmp = rand() % 100 + 1; // Generates random integer between 1 and 100
      a.makeNode(tmp);  // Creates a new node in a linked list
   }
   a.showList(); // Displays node values in order generated
   cout << endl;
   a.selectionSort();
   a.showList();
   cout << endl;
   return 0;
}
